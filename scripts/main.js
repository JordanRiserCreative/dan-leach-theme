//@codekit-prepend "scripts/jquery-1.9.1-min.js";
//@codekit-prepend "scripts/browser.js";

if(!Modernizr.touch) {
    $('.logo').waypoint(function(dir){
        if(dir == 'down'){
            $('.logo').hide();
            $('header').show();
        }else{
            $('.logo').show();
            $('header').hide();
        }
    },{offset: '24px'});
}