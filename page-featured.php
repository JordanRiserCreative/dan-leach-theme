<?php
get_header();
/*
Template Name: Home Page - Feature
*/
?>
<header class="featured-header">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Dan Leach">
</header>
<section id="featured-publication" class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-4 book-cover">
			<img src="<?php the_field('book_cover'); ?>">
		</div>
		<div class="col-xs-12 col-sm-8 pic">
			<?php the_field('book_content'); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>