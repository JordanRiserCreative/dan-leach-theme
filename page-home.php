<?php
get_header();
/*
Template Name: Home Page
*/
?>
<section id="top">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Dan Leach" class="logo">
	<header>
		<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Dan Leach">
	</header>
</section>
<?php if(get_field('show_featured_book')): ?>
<section id="featured-publication" class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-4 book-cover">
			<img src="<?php the_field('book_cover'); ?>">
		</div>
		<div class="col-xs-12 col-sm-8 pic">
			<?php the_field('book_content'); ?>
		</div>
	</div>
</section>
<?php endif; ?>
<?php if(have_posts()): ?>
<section id="bio" class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-4 col-sm-offset-1 bio">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					the_content();
				endwhile;
			?>
		</div>
		<div class="col-xs-12 col-sm-4 col-sm-offset-1 pic">
		<?php echo get_the_post_thumbnail( $page->ID, 'full' ); ?>
		<!-- <section id="about-main" <?php echo 'style="background-image:url('.$url.')"'; ?>> -->
			<!-- <img src="<?php echo get_template_directory_uri(); ?>/img/dan-leach.jpg" alt="Dean Leach" class="bio-pic"> -->
		</div>
	</div>
</section>
<?php endif; ?>
<?php
$args=array(
	'post_type' => 'link',
	'category_name' => 'story',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'caller_get_posts'=> 1
);
$query = new WP_Query($args); ?>
<?php if($query->have_posts() && get_field('show_links')): ?>
<section id="links">
	<div class="row">
		<div class="col-sm-12 col-md-6">
			<h3>Stories</h3>

			<?php while ($query->have_posts()) : $query->the_post();
				?>
				<div class="link">
					<h2><?php the_title(); ?></h2>
					<span>
						<a href="<?php the_cfc_field('url', 'url'); ?>" target="new"><?php the_cfc_field('url', 'published-by'); ?></a>
						<i><?php the_cfc_field('date', 'publish-date'); ?></i>
					</span>
				</div>
				<?php
			endwhile;
			 ?>
		</div>
		<div class="col-sm-12 col-md-6">
			<h3>Poems</h3>
			<?php
			$args=array(
				'post_type' => 'link',
				'category_name' => 'poem',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'caller_get_posts'=> 1
			);
			$query = new WP_Query($args);

			while ($query->have_posts()) : $query->the_post();
				?>
				<div class="link">
					<h2><?php the_title(); ?></h2>
					<span>
						<a href="<?php the_cfc_field('url', 'url'); ?>" target="new"><?php the_cfc_field('url', 'published-by'); ?></a>
						<i><?php the_cfc_field('date', 'publish-date'); ?></i>
					</span>
				</div>
				<?php
			endwhile;
			 ?>
		</div>
	</div>
</section>
<?php endif; ?>
<?php get_footer(); ?>