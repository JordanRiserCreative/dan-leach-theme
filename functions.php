<?php

function register_theme_styles() {
	wp_register_style( 'theme-styles', get_template_directory_uri().'/css/site.css');
	wp_enqueue_style( 'theme-styles' );
}
add_action( 'wp_enqueue_scripts', 'register_theme_styles' );

add_theme_support( 'post-thumbnails' ); 

function dan_leach_scripts() {
	wp_register_script('main', get_template_directory_uri().'/js/main.min.js', array('jquery'),'1.0', true);

	wp_enqueue_script('main');
}
add_action( 'wp_enqueue_scripts', 'dan_leach_scripts' );  