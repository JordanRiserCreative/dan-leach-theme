<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<!-- <link rel="icon" type="image/png" href="img/favicon.png"> -->
	<!-- <link rel="stylesheet" href="css/style.css"> -->
	<?php wp_head(); ?>
</head>
<body>